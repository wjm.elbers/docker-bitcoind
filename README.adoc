= docker-bitcoind
:caution-caption: ☡ CAUTION
:important-caption: ❗ IMPORTANT
:note-caption: 🛈 NOTE
:sectanchors:
:sectlinks:
:sectnumlevels: 6
:sectnums:
:source-highlighter: pygments
:tip-caption: 💡 TIP
:toc-placement: preamble
:toc:
:warning-caption: ⚠ WARNING

This project provides a Docker container image.
Using a container based on this image, you can run an reverse proxy using https://httpd.apache.org/[Apache httpd web server].

== To use

The best way to discover how to use the image is by reading the link:docker-compose.yml[`docker-compose.yml`] and link:deployment.yml[`deployment.yml`] files.
These specify how to deploy containers based on this image with Docker Compose.
link:build.sh[`build.sh`] contains information about the commands needed to deploy.

== To build and test

The build, test and release pipeline is specified in the the link:.gitlab-ci.yml[GitLab CI] config file.
This pipeline or its stages can be run by GitLab.com CI as well as by a self-hosted GitLab instance.
This is elaborated in the https://about.gitlab.com/gitlab-ci/[GitLab CI docs].
In addition, the build and test stages can be run be run locally within a bare Docker container, without a GitLab CI pipeline.

NOTE: Please issue all of the following shell statements from within the root directory of this repository.

=== Without a GitLab CI pipeline

[source,sh]
----
sh build.sh --local --build
----