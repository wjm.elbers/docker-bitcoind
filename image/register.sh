#!/bin/bash

if [ -z ${REPORTING_ENDPOINT_SCHEME+x} ]; then
    echo "REPORTING_ENDPOINT_SCHEME not set"
    exit 1
fi
if [ -z ${REPORTING_ENDPOINT_HOST+x} ]; then
    echo "REPORTING_ENDPOINT_HOST not set"
    exit 1
fi
if [ -z ${REPORTING_ENDPOINT_PORT+x} ]; then
    echo "REPORTING_ENDPOINT_PORT not set"
    exit 1
fi
if [ -z ${COIN+x} ]; then
    echo "COIN not set"
    exit 1
fi


#Decide if RPC is enabled
RPC_ENABLED="1"
if [ -z ${RPC_USER+x} ]; then
    RPC_ENABLED="0"
fi
if [ -z ${RPC_PASSWORD+x} ]; then
    RPC_ENABLED="0"
fi

if [ -z ${SEGWIT+x} ]; then
    SEGWIT="false"
fi

hostname=$(hostname)
data=""
if [ "${RPC_ENABLED}" == "1" ]; then
    data="{\"rpc\": 1, \"hostname\": \"${hostname}\", \"rpc_port\": ${RPC_PORT}, \"segwit\": ${SEGWIT}}"
else
    data="{\"rpc\": 0, \"hostname\": \"${hostname}\"}"
fi



echo "$(date) webhook - register" >> /var/log/webhook/webhook.log
URL="${REPORTING_ENDPOINT_SCHEME}://${REPORTING_ENDPOINT_HOST}:${REPORTING_ENDPOINT_PORT}/${COIN}/register"
curl -s "${URL}" -d "${data}" >> /var/log/webhook/webhook.log 2>&1